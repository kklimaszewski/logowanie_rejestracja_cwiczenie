from django import forms
from .models import Account, Comment


class AccountForm(forms.ModelForm):
    password = forms.CharField(max_length=128, widget=forms.PasswordInput())
    confirm_password = forms.CharField(max_length=128, widget=forms.PasswordInput())
    class Meta:
        model = Account
        fields = ['email', 'password', 'confirm_password','first_name','last_name']


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=30, required=True)
    message = forms.CharField(max_length=300, widget=forms.Textarea(attrs={'rows': 10}), required=True)

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']