# -*- coding: utf-8 -*-
import os

from aplikacja.settings import BASE_DIR

__author__ = 'karol'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}