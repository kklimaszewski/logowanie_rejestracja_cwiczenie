from django.contrib import admin


from .models import Account
from .forms import AccountForm

class AccountAdmin(admin.ModelAdmin):
    list_display = ["__unicode__", "activation_key", "first_name", "last_name", "timestamp", "update", "is_admin", "is_active", "is_superuser", "is_staff"]
    form = AccountForm

admin.site.register(Account, AccountAdmin)