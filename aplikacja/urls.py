from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin



urlpatterns = [
    # Examples:
    url(r'^$', 'aplikacja.views.home', name='home'),
    url(r'^comment/', 'aplikacja.views.comment', name='comment'),
    url(r'^contact/', 'aplikacja.views.contact', name='contact'),
    url(r'^registration/', 'aplikacja.views.registration', name='registration'),
    url(r'^account/(?P<activation_key>[\w]+)/', 'aplikacja.views.autoryzacja', name='autoryzacja'),
    url(r'^login/', 'aplikacja.views.login', name='login'),
    url(r'^logout/', 'aplikacja.views.logout_view', name='logout'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)














