# -*- coding: utf-8 -*-
import hashlib

from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
from django.contrib.auth import logout, REDIRECT_FIELD_NAME, login as auth_login
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from django.template.response import TemplateResponse
from django.utils.crypto import random
from django.utils.http import is_safe_url

__author__ = '$USER'

from django.shortcuts import render, resolve_url, get_object_or_404, redirect
from .forms import AccountForm, ContactForm, CommentForm
from .models import Account, Comment


def home(request):
    if request.user.is_authenticated():
        name = request.user.email

        context = {
            "name": name
        }

        return render(request, "home.html", context)
    else:
        return render(request, "home.html")



def contact(request):

    if request.user.is_authenticated():
        name = request.user.email
        form = ContactForm(request.POST or None)
        title = "Contact Us"

        context = {
                "name": name,
                "form": form,
                "title": title
            }


        if form.is_valid():
            title = "Wiadomość została wysłana. Dziękujemy!"
            subject = form.cleaned_data["subject"]
            contact_message = form.cleaned_data["message"]
            from_email = request.user.email
            to_email = settings.EMAIL_HOST_USER
            send_mail(subject,
                      contact_message,
                      from_email,
                      [to_email],
                      fail_silently=True)

            context = {
                "name": name,
                "form": form,
                "title": title
            }

            return render(request, "contact.html", context)

        return render(request, "contact.html", context)


    else:
        title = "Panel wysyłania wiadomości dostepny po zalogowaniu się."
        context = {
                "title": title
            }



        return render(request, "contact.html", context)



def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():

            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)
    current_site = get_current_site(request)
    title = "Logowanie"
    context = {
        'title': title,
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)

def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")


def autoryzacja(request, activation_key):

    account = Account.objects.get(activation_key=activation_key)
    account.is_active="True"
    account.save()


    title = "Rejestracja przebiegła pomyślnie"

    form = AccountForm(request.POST or None)
    context = {
        "title": title,
        "form": form,
    }

    return render(request, "registration/registration.html", context)

def registration(request):

    title = "Rejestracja"

    form = AccountForm(request.POST or None)
    context = {
        "title": title,
        "form": form
    }

    if form.is_valid():

        instance = form.save(commit=False)
        if form.cleaned_data["password"] == form.cleaned_data["confirm_password"]:


            salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
            email = form.cleaned_data["email"]
            if isinstance(email, unicode):
                email = email.encode('utf-8')
            activation_key = hashlib.sha1(salt+email).hexdigest()

            instance.set_password(form.cleaned_data["password"])
            instance.activation_key=activation_key
            instance.save()

            context = {
                "title": "Prosimy o autoryzację przez link wysłany do Państwa na maila.",
                "form": form
            }

            subject = "Serwis Klimka"
            message = "Witamy w naszym serwisie. W celu dokończenia rejestracji proszę kliknąć link: http://127.0.0.1:8000/account/" + activation_key + "/"
            email_from = "klimek234@wp.pl"
            email_to = form.cleaned_data["email"]

            send_mail(subject, message, email_from, [email_to], fail_silently=False)

        else:

            context = {
                "title": "Podane hasła roznia sie od siebie!",
                "form": form
            }




    return render(request, "registration/registration.html", context)



def comment(request):
    newsletter = Comment.objects.all()
    form = CommentForm(request.POST or None)

    context = {
            'newsletter': newsletter,
            'form': form
        }

    if form.is_valid():

        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()

        context = {
            'newsletter': newsletter,
            'form': form
        }


        return redirect('/comment/', request, 'comment.html', context)

    return render(request, 'comment.html', context)




